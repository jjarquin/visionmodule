#include <signal.h>
#include <boost/shared_ptr.hpp>
#include <alcommon/albroker.h>
#include <alcommon/almodule.h>
#include <alcommon/albrokermanager.h>
#include <alcommon/altoolsmain.h>
#include "vision_module.h"

#ifdef _WIN32
# define ALCALL __declspec(dllexport)
#else
# define ALCALL
#endif

extern "C"
{
  ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
  {
    // init broker with the main broker instance
    // from the parent executable
    AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(pBroker);

    // create module instances
    AL::ALModule::createModule<VisionModule>(pBroker,"VisionModuleTest");
    return 0;
  }

  ALCALL int _closeModule()
  {
    return 0;
  }

} // extern "C"

#ifdef LOCOMOTION_IS_REMOTE
  int main(int argc, char *argv[])
  {

      if(argc < 2)
      {
        std::cerr << "Usage: custommain NAO_IP" << std::endl;
        return 2;
      }

    // pointer to createModule
    TMainType sig;
    sig = &_createModule;
    // call main
    return ALTools::mainFunction("VisionModuleTest", argc, argv, sig);
  }
#endif
