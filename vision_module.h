#ifndef VISION_MODULE_H
#define VISION_MODULE_H

#include <alcommon/almodule.h>
#include <boost/shared_ptr.hpp>

#include <string>

#include <alproxies/alvideodeviceproxy.h>
#include <alvision/alimage.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <vector>
#include <pthread.h>
#include <time.h>

namespace AL
{
  class ALBroker;
}

class VisionModule : public AL::ALModule
{
public:

  VisionModule(boost::shared_ptr<AL::ALBroker> broker, const std::string& name);

  virtual ~VisionModule();

  void init();
  void processImage();
  void onProcessImage();
  void onProcessImageQVGA();
  void startTracking();
  void stopTracking();
  void resetImages();
  void setCurrentFeatures(const std::vector<float> & newCurrentFeatures);
  void setTargetFeatures(const std::vector<float> & newTargetFeatures);
  std::vector<float> getCurrentFeatures();
  std::vector<float> getTargetFeatures();
  std::vector<float> getFeatureError();

protected:
  std::vector<cv::Point2f> calculateError(const std::vector<cv::Point2f> & currentFeatureVector, const std::vector<cv::Point2f> & targetFeatureVector);
private:
  pthread_t trackingThread;

  static void * trackingFunction(void *ptr){
    VisionModule * args = ( VisionModule *) ptr;
    struct timespec time;
    args->trackerWinSize = cv::Size(31,31);
    args->trackerTermcrit = cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
    while(args->running){
        time.tv_sec = 0;
        //time.tv_nsec =  5000000; // 10ms
        time.tv_nsec =  10000000; // 10ms
        //time.tv_nsec = 100000000; // 100ms
        nanosleep(&time,0);
        args->onProcessImage();
      }
    return NULL;
  }
  bool running;

  const int kFEATURES_VECTOR_SIZE;

  boost::shared_ptr<AL::ALVideoDeviceProxy> cameraProxy;
  AL::ALImage * imagePointer;
  cv::Mat previousImage;
  cv::Mat currentImage;

  cv::Mat previousImageQVGA;
  cv::Mat currentImageQVGA;

  cv::Mat imageYUV;

  bool cameraOnline;
  bool currentFeaturesSet;
  bool targetFeaturesSet;
  bool featuresReady;
  bool featureErrorReady;

  std::vector<cv::Point2f> targetFeatures;
  std::vector<cv::Point2f> currentFeatures;
  std::vector<cv::Point2f> nextFeatures;
  std::vector<cv::Point2f> featureError;

  std::vector<uchar> trackerStatus;
  std::vector<float> trackerErr;
  cv::Size trackerWinSize;
  cv::TermCriteria trackerTermcrit;


  std::string moduleId;

  int resolution;
  int colorSpace;
  int fps;

};

#endif // VISION_MODULE_H
