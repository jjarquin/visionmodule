#include "vision_module.h"
#include <alcommon/almodule.h>
#include <alvalue/alvalue.h>
#include <alcommon/alproxy.h>
#include <alcommon/albroker.h>
#include <iostream>

#include <alvision/alvisiondefinitions.h>
#include "vision_utils.h"
#include <boost/timer/timer.hpp>


#include <iostream>

using std::cout;
using std::endl;
using boost::timer::cpu_timer;


VisionModule::VisionModule(boost::shared_ptr<AL::ALBroker> broker, const std::string &name)
  : AL::ALModule(broker, name) , cameraProxy(boost::shared_ptr<AL::ALVideoDeviceProxy>(new AL::ALVideoDeviceProxy(getParentBroker()))) , moduleId("VisionModule"), kFEATURES_VECTOR_SIZE(8)
  , imageYUV(cv::Mat(cv::Size(640, 480), CV_8UC2)), currentImage(cv::Mat(cv::Size(640, 480) , CV_8U)), previousImage(cv::Mat(cv::Size(640, 480), CV_8U))
{
  setModuleDescription("Vision Module for Points.");
  cameraOnline = false;
  currentFeaturesSet = false;
  targetFeaturesSet = false;
  featuresReady = false;
  featureErrorReady = false;


  currentImageQVGA = cv::Mat(cv::Size(320, 240), CV_8U);
  previousImageQVGA = cv::Mat(cv::Size(320, 240), CV_8U);

  functionName("processImage", getName() , "Extract an image from the camera and process it");
  BIND_METHOD(VisionModule::processImage);

  functionName("resetImages",getName(), "Reset both current and next images to the actual image.");
  BIND_METHOD(VisionModule::resetImages);

  functionName("startTracking",getName(), "Start internal tracking.");
  BIND_METHOD(VisionModule::startTracking);

  functionName("stopTracking",getName(), "Stop internal tracking.");
  BIND_METHOD(VisionModule::stopTracking);


  functionName("getCurrentFeatures", getName() , "Get the current features.");
  setReturn("currentFeatures", "A vector of floats with the current features.");
  BIND_METHOD(VisionModule::getCurrentFeatures);

  functionName("getTargetFeatures", getName() , "Get the target features.");
  setReturn("targetFeatures", "A vector of floats with the target features.");
  BIND_METHOD(VisionModule::getTargetFeatures);

  functionName("getFeatureError", getName() , "Get the error of the features.");
  setReturn("featureError", "A vector of floats with the visual error.");
  BIND_METHOD(VisionModule::getFeatureError);

  functionName("setCurrentFeatures", getName() , "Set the current features.");
  addParam("currentFeatures","A vector of floats with the current features.");
  BIND_METHOD(VisionModule::setCurrentFeatures);

  functionName("setTargetFeatures", getName() , "Get the target features.");
  addParam("targetFeatures","A vector of floats with the target features.");
  BIND_METHOD(VisionModule::setTargetFeatures);




}

void VisionModule::init(){
  cout << "Init VisionModule" << endl;

  // Then specify the resolution among : kQQVGA (160x120), kQVGA (320x240),
  // kVGA (640x480) or k4VGA (1280x960, only with the HD camera).
  // (Definitions are available in alvisiondefinitions.h)
  resolution = AL::kVGA;

  // Then specify the color space desired among : kYuvColorSpace, kYUVColorSpace,
  // kYUV422ColorSpace, kRGBColorSpace, etc.
  // (Definitions are available in alvisiondefinitions.h)
  colorSpace = AL::kRGBColorSpace;

  // Finally, select the minimal number of frames per second (fps) that your
  // vision module requires up to 30fps.
  fps = 10;

  running = false;

  try{
    moduleId = cameraProxy->subscribeCamera(moduleId,0,resolution,colorSpace,fps);

    imagePointer = ( AL::ALImage* ) cameraProxy->getDirectRawImageLocal(moduleId);
    imageYUV.data = imagePointer->getData();

    cv::extractChannel(imageYUV,currentImage,0);
    cameraProxy->releaseDirectRawImage(moduleId);
    currentImage.copyTo(previousImage);
    cv::resize(previousImage,previousImageQVGA,cv::Size(320,240));

  }
  catch(...){
    std::cout << "Error while connecting to the camera Proxy" << std::endl;
  }
  std::cout << "Vision Module initialized"<< std::endl;

}

void VisionModule::setCurrentFeatures(const std::vector<float> & newCurrentFeatures){
  assert(newCurrentFeatures.size() == kFEATURES_VECTOR_SIZE);
  floatVector2cvPointVector(newCurrentFeatures,currentFeatures);
  currentFeaturesSet = true;

  std::cout <<  "Getting... [ " ;
  for(int i = 0 ; i != newCurrentFeatures.size() ; i++){
      std::cout <<  newCurrentFeatures[i] << " ";
    }
  std::cout << "]" << std::endl;
}

void VisionModule::setTargetFeatures(const std::vector<float> & newTargetFeatures){
  assert(newTargetFeatures.size() == kFEATURES_VECTOR_SIZE);
  floatVector2cvPointVector(newTargetFeatures,targetFeatures);
  targetFeaturesSet = true;


  std::cout <<  "Getting... [ " ;
  for(int i = 0 ; i != newTargetFeatures.size() ; i++){
      std::cout <<  newTargetFeatures[i] << " ";
    }
  std::cout << "]" << std::endl;
}
std::vector<float> VisionModule::getCurrentFeatures(){
  std::vector<float> current;

  if(featuresReady){
      cvPointVector2FloatVector(currentFeatures,current);
      return current;

    }
  else if(currentFeaturesSet){
      cvPointVector2FloatVector(currentFeatures,current);
      return current;

    }
  else{
      return current;

    }
}

std::vector<float> VisionModule::getFeatureError(){
  std::cout << "Sending featureError" << std::endl;

  std::vector<float> error;

  if(featureErrorReady){
      cvPointVector2FloatVector(featureError,error);
      return error;

    }
  else{
      return error;
    }
}

std::vector<float> VisionModule::getTargetFeatures(){
  std::vector<float> target;

  if(targetFeaturesSet){
      cvPointVector2FloatVector(targetFeatures,target);
      return target;
    }
  else{
      return target;

    }
}

void VisionModule::resetImages(){
  std::cout << "Reset images" << std::endl;

  imagePointer = ( AL::ALImage* ) cameraProxy->getDirectRawImageLocal(moduleId);
  imageYUV.data = imagePointer->getData();
  cv::extractChannel(imageYUV,currentImage,0);
  cameraProxy->releaseDirectRawImage(moduleId);
  currentImage.copyTo(previousImage);
  cv::resize(previousImage,previousImageQVGA,cv::Size(320,240));

  currentFeaturesSet = false;

}

std::vector<cv::Point2f> VisionModule::calculateError(const std::vector<cv::Point2f> & currentFeatureVector, const std::vector<cv::Point2f> & targetFeatureVector){
  //std::cout << "Computing error" << std::endl;
  std::vector<cv::Point2f> error;
  cv::Point2f tempPointError;
  for(int i = 0 ; i!= (kFEATURES_VECTOR_SIZE/2); i++){
      tempPointError = currentFeatureVector[i] - targetFeatureVector[i];
      error.push_back(tempPointError);
    }
  return error;

}

void VisionModule::processImage(){
  cout << "Processing Image" << endl;
  cpu_timer timer;
  featuresReady = false;
  featureErrorReady = false;
  std::vector<uchar> status;
  std::vector<float> err;
  cv::Size winSize(41,41);
  cv::TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);

  if(currentFeaturesSet){
      imagePointer = ( AL::ALImage* ) cameraProxy->getDirectRawImageLocal(moduleId);
      imageYUV.data = imagePointer->getData();
      cv::extractChannel(imageYUV,currentImage,0);
      cameraProxy->releaseDirectRawImage(moduleId);

      timer.start();

      cv::calcOpticalFlowPyrLK(previousImage,currentImage,currentFeatures,nextFeatures,status, err, winSize,3,termcrit,0.5,0,0.001);
      timer.stop();


      featuresReady = true;
      if(targetFeaturesSet){
          featureError = calculateError(currentFeatures,targetFeatures);
          featureErrorReady = true;

        }

      cv::swap(currentImage,previousImage);
      std::swap(currentFeatures,nextFeatures);
    }
  cout << "Computation time" << endl << timer.format() << endl;

  cv::Mat imgCur = cv::Mat(cv::Size(320, 240), CV_8U);
  cv::Mat imgPro = cv::Mat(cv::Size(320, 240), CV_8U);

  cv::Size size = cv::Size(320,240);
  timer.start();

  cv::resize(currentImage,imgPro,size);
  cv::resize(previousImage,imgCur,size);
  std::vector<cv::Point2f> currentFeats;
  std::vector<cv::Point2f> nextFeats;
  timer.stop();
  cout << "Resizing time 320x240 " << endl << timer.format() << endl;

  for(int i = 0 ; i!= currentFeatures.size() ; i ++) {
      currentFeats.push_back(cv::Point2f(nextFeatures[i].x/2.0,nextFeatures[i].y/2.0));
      nextFeats.push_back(cv::Point2f(currentFeatures[i].x/2.0,currentFeatures[i].y/2.0));
   }
  timer.start();

  cv::calcOpticalFlowPyrLK(imgPro,imgCur,currentFeats,nextFeats,status, err, winSize,3,termcrit,0.5,0,0.001);
  timer.stop();
  cout << "Computation time 320x240 " << endl << timer.format() << endl;

  std::cout << "next [ " ;
  for(int i = 0 ; i!= currentFeats.size() ; i ++) {
      std::cout << nextFeats[i] << "  " ;

   }
  std::cout << " ] " << std::endl;

  std::cout << "current [ " ;
  for(int i = 0 ; i!= currentFeats.size() ; i ++) {
      std::cout << currentFeats[i] << "  " ;

   }
  std::cout << " ] " << std::endl;




}


void VisionModule::startTracking(){
  if(!running & currentFeaturesSet & targetFeaturesSet){
      running = true;
     pthread_create( &trackingThread, NULL, trackingFunction, (void *) this);
    }
}

void VisionModule::stopTracking(){
    running = false;
}

void VisionModule::onProcessImage(){
  cpu_timer timer;

  timer.start();
  imagePointer = ( AL::ALImage* ) cameraProxy->getDirectRawImageLocal(moduleId);
  imageYUV.data = imagePointer->getData();
  cv::extractChannel(imageYUV,currentImage,0);
  cameraProxy->releaseDirectRawImage(moduleId); //posibble problem with memory as we are using a thread
//  std::vector<uchar> status;
//  std::vector<float> err;
//  cv::Size winSize(41,41);
//  cv::TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
//  cv::calcOpticalFlowPyrLK(previousImage,currentImage,currentFeatures,nextFeatures,status, err, winSize,3,termcrit,0.5,0,0.001);
  cv::calcOpticalFlowPyrLK(previousImage,currentImage,currentFeatures,nextFeatures,trackerStatus, trackerErr, trackerWinSize,3,trackerTermcrit,0.5,0,0.001);
  currentImage.copyTo(previousImage);
  //cv::swap(currentImage,previousImage);
  std::swap(currentFeatures,nextFeatures);
  timer.stop();

  cout << "Computation time 640x480 " << endl << timer.format() << endl;

}

void VisionModule::onProcessImageQVGA(){
  cpu_timer timer;

  timer.start();

  imagePointer = ( AL::ALImage* ) cameraProxy->getDirectRawImageLocal(moduleId);
  imageYUV.data = imagePointer->getData();
  cv::extractChannel(imageYUV,currentImage,0);

  cv::resize(currentImage,currentImageQVGA,cv::Size(320,240));


  std::vector<cv::Point2f> currentFeats;
  std::vector<cv::Point2f> nextFeats;

  for(int i = 0 ; i!= currentFeatures.size() ; i ++) {
      currentFeats.push_back(cv::Point2f(currentFeatures[i].x/2.0,currentFeatures[i].y/2.0));
   }

  cameraProxy->releaseDirectRawImage(moduleId); //posibble problem with memory as we are using a thread

  cv::calcOpticalFlowPyrLK(previousImageQVGA,currentImageQVGA,currentFeats,nextFeats,trackerStatus, trackerErr, trackerWinSize,3,trackerTermcrit,0.5,0,0.001);

  nextFeatures.clear();
  for(int i = 0 ; i!= currentFeatures.size() ; i ++) {
      nextFeatures.push_back(cv::Point2f(nextFeats[i].x*2.0,nextFeats[i].y*2.0));
   }
  cv::swap(currentImage,previousImage);
  cv::swap(currentImageQVGA,previousImageQVGA);
  std::swap(currentFeatures,nextFeatures);
  timer.stop();
  cout << "Computation time 320x240 " << endl << timer.format() << endl;
}


VisionModule::~VisionModule() {
  delete imagePointer;

}
