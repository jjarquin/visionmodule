VisionModule is a LocalModule for the humanoid robot Nao. This module tracks visual features at real time (~15Hz) and provides an interface to access the data.

It is currently in the development phase.


Todo: Build instructions

* Configuration
* Dependencies
* Install

If you are interested in contributing to the project contact:
Julio Jarquin - jjarquin.ct@gmail.com