#ifndef VISION_UTILS_H
#define VISION_UTILS_H

#include <opencv2/core/core.hpp>
#include <vector>

void floatVector2cvPointVector(const std::vector<float> & src, std::vector<cv::Point2f> & dest){
    assert(src.size() > 1);
    dest.clear();
    for(int i = 0 ; i < src.size(); i = i + 2){
        dest.push_back(cv::Point2f(src[i],src[i+1]));
    }
}

void cvPointVector2FloatVector(const std::vector<cv::Point2f> & src, std::vector<float> & dest){
    assert(src.size() > 0);
    dest.clear();
    for(int i = 0 ; i != src.size(); i ++){
        dest.push_back( src[i].x);
        dest.push_back( src[i].y);
    }
}

#endif // VISION_UTILS_H
